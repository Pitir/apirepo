using BaseTypes;

namespace Server.Data;

public class User
{
    public int Id { get; set; }
    
    public string Name { get; set; }
    
    public Status Status { get; set; }
}
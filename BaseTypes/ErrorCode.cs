using System.ComponentModel;

namespace BaseTypes;

public enum ErrorCode
{
    [Description("")]
    NotError = 0,
    
    [Description("User with id {0} already exist")]
    UserAlreadyExist = 1,
    
    [Description("User not found")]
    UserNotFound = 2,
    
    [Description("General error")]
    UndefinedError = 3
}
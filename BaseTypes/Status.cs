﻿namespace BaseTypes;

public enum Status
{
    New = 0,
    Active = 1,
    Blocked = 2,
    Deleted = 3
}
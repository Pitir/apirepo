﻿namespace BaseTypes;

public class UserInfo
{
    public int Id { get; set; }
    
    public string Name { get; set; }
    
    public Status Status { get; set; }
}
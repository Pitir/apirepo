using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Server.Contract;
using Server.Data;
using Server.Implementations;
using Server.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(options =>
    {
        options.OutputFormatters.Add(new XmlSerializerOutputFormatterNamespace());
    })
    .AddXmlSerializerFormatters();

builder.Services.AddAuthentication("BasicAuthentication").
    AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>
        ("BasicAuthentication", null);

builder.Services.Configure<ServerSettingsOptions>(
    builder.Configuration.GetSection(ServerSettingsOptions.ServerSettings));

builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddScoped<IAuthorizationService, AuthorizationService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddSingleton<IApiState, ApiState>();

builder.Services.AddHostedService<CacheHostedService>();

builder.Services.AddDbContext<ApiDbContext>(p 
    => p.UseMySQL(builder.Configuration.GetConnectionString("ApiDatabase")));

builder.Services.AddMvc();

var app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
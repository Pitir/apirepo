using BaseTypes;
using Server.Models.Responses;

namespace Server.Contract;

public interface IUserService
{
    public UserInfoResponse Create(UserInfo userInfo);

    public UserInfoResponse SetStatus(int id, Status newStatus);

    public UserInfo[] GetAll();
}
using System.Collections.Concurrent;
using BaseTypes;

namespace Server.Contract;

public interface IApiState
{
    public ConcurrentDictionary<int, UserInfo> UserInfos { get; set; }
}
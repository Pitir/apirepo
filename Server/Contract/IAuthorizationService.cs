using Server.Models;

namespace Server.Contract;

public interface IAuthorizationService
{
    Task<AuthorizeUser> Authenticate(string username, string password);
}
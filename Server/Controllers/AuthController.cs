using AutoMapper;
using BaseTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Contract;
using Server.Models.Requests;
using Server.Models.Responses;

namespace Server.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]/[action]")]
public class AuthController : ControllerBase
{
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public AuthController(IUserService userService, IMapper mapper)
    {
        _userService = userService;
        _mapper = mapper;
    }

    [HttpPost]
    [Consumes("application/xml")]
    [Produces("application/xml")]
    public CreateUserResponseBase CreateUser(CreateUserRequest request)
    {
        CreateUserResponseBase createUserResponse;

        try
        {
            var userInfo = _mapper.Map<UserInfo>(request.CreateUser);
            var createdUser = _userService.Create(userInfo);

            if (createdUser.ErrorCode == ErrorCode.NotError)
            {
                var user = _mapper.Map<CreateUser>(createdUser.UserInfo);

                createUserResponse = new CreateUserResponseSuccess(user, createdUser.ErrorCode);
            }
            else
                createUserResponse = new CreateUserResponseError(createdUser.ErrorCode, request.CreateUser.Id);

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            createUserResponse = new CreateUserResponseError(ErrorCode.UndefinedError);
        }

        return createUserResponse;
    }
    
    [HttpPost]
    public RemoveUserResponseBase RemoveUser(RemoveUserRequest request)
    {
        RemoveUserResponseBase removeUserResponse;
        try
        {
            var removedUser = _userService.SetStatus(request.RemoveUser.Id, Status.Deleted);

            if (removedUser.ErrorCode == ErrorCode.NotError)
                removeUserResponse = new RemoveUserResponseSuccess(removedUser.UserInfo, "User was removed");
            else
                removeUserResponse = new RemoveUserResponseError(removedUser.ErrorCode);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            removeUserResponse = new RemoveUserResponseError(ErrorCode.UndefinedError);
        }

        return removeUserResponse;
    }
    
    [HttpPost]
    public UserInfo SetStatus([FromForm]int id, [FromForm]Status newStatus)
    {
        var userInfo = new UserInfo();
        try
        {
            var updatedUser = _userService.SetStatus(id, newStatus);

            if (updatedUser.ErrorCode == ErrorCode.NotError)
                userInfo = updatedUser.UserInfo;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return userInfo;
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Server.Contract;

namespace Server.Controllers;

[Route("[controller]/[action]")]
public class PublicController : Controller
{
    private readonly IApiState _apiState;

    public PublicController(IApiState apiState)
    {
        _apiState = apiState;
    }

    [HttpGet]
    public IActionResult UserInfo(int id)
    {
        _apiState.UserInfos.TryGetValue(id, out var user);
        return View(user);
    }
}
using System.Xml.Serialization;
using BaseTypes;
using Server.Infrastructure;

namespace Server.Models.Responses;

public class CreateUserResponseError : CreateUserResponseBase
{
    public CreateUserResponseError()
    {
    }
    
    public CreateUserResponseError(ErrorCode errorCode, params object[] parameters) : base(errorCode, false)
    {
        ErrorMsg = string.Format(errorCode.GetDescription(), parameters);
    }

    [XmlElement(ElementName="ErrorMsg")]
    public string ErrorMsg { get; set; }
}
﻿using System.Xml.Serialization;
using BaseTypes;

namespace Server.Models.Responses;

[XmlInclude(typeof(CreateUserResponseSuccess))]
[XmlInclude(typeof(CreateUserResponseError))]
[XmlRoot(ElementName="Response")]
public abstract class CreateUserResponseBase 
{
    protected CreateUserResponseBase()
    {
    }
    
    protected CreateUserResponseBase(ErrorCode errorCode, bool success)
    {
        ErrorId = (int)errorCode;
        Success = success;
    }
    
    [XmlAttribute(AttributeName="Success")]
    public bool Success { get; set; }
    
    [XmlAttribute(AttributeName="ErrorId")]
    public int ErrorId { get; set; }
}
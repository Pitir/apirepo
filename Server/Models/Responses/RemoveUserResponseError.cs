using BaseTypes;
using Server.Infrastructure;

namespace Server.Models.Responses;

public class RemoveUserResponseError : RemoveUserResponseBase
{
    public RemoveUserResponseError()
    {
    }
    
    public RemoveUserResponseError(ErrorCode errorCode) : base(false, errorCode.GetDescription())
    {
        ErrorId = (int)errorCode;
    }
    
    public int ErrorId { get; set; }
}
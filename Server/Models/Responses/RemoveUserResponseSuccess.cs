using BaseTypes;

namespace Server.Models.Responses;

public class RemoveUserResponseSuccess : RemoveUserResponseBase
{
    public RemoveUserResponseSuccess()
    {
    }
    
    public RemoveUserResponseSuccess(UserInfo userInfo, string msg) : base(true, msg)
    {
        User = userInfo;
    }
    
    public UserInfo User { get; set; }
}
using System.Xml.Serialization;
using BaseTypes;
using Server.Models.Requests;

namespace Server.Models.Responses;

public class CreateUserResponseSuccess : CreateUserResponseBase
{
    public CreateUserResponseSuccess()
    {
    }
    
    public CreateUserResponseSuccess(CreateUser createUser, ErrorCode errorCode) : base(errorCode, true)
    {
        CreateUser = createUser;
    }
    
    [XmlElement(ElementName="user")]
    public CreateUser CreateUser { get; set; }
}
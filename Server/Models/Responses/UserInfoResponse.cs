using BaseTypes;

namespace Server.Models.Responses;

public class UserInfoResponse
{
    public ErrorCode ErrorCode { get; set; }
    
    public UserInfo UserInfo { get; set; }
}
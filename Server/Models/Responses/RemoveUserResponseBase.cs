﻿namespace Server.Models.Responses;

public abstract class RemoveUserResponseBase
{
    protected RemoveUserResponseBase()
    {}

    protected RemoveUserResponseBase(bool success, string msg)
    {
        Msg = msg;
        Success = success;
    }
    
    public string Msg { get; set; }
    public bool Success { get; set; }
}
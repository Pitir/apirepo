using System.Xml.Serialization;
using BaseTypes;

namespace Server.Models.Requests;

[XmlRoot(ElementName="user")]
public class CreateUser {
    [XmlElement(ElementName="Status")]
    public Status Status { get; set; }
    [XmlAttribute(AttributeName="Id")]
    public int Id { get; set; }
    [XmlAttribute(AttributeName="Name")]
    public string Name { get; set; }
}
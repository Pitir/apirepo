﻿namespace Server.Models.Requests;

public class RemoveUserRequest
{
    public RemoveUser RemoveUser { get; set; }
}
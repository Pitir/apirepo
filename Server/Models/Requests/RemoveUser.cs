namespace Server.Models.Requests;

public class RemoveUser
{
    public int Id { get; set; }
}
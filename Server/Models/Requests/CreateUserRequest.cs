﻿using System.Xml.Serialization;

namespace Server.Models.Requests;

[XmlRoot(ElementName="Request")]
public class CreateUserRequest {
    [XmlElement(ElementName="user")]
    public CreateUser CreateUser { get; set; }
}
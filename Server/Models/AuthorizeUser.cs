namespace Server.Models;

public class AuthorizeUser
{
    public string Id { get; set; }
    public string Username { get; set; }
}
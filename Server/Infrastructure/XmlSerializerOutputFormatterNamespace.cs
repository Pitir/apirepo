﻿using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace Server.Infrastructure;

public class XmlSerializerOutputFormatterNamespace : XmlSerializerOutputFormatter
{
    protected override void Serialize(XmlSerializer xmlSerializer, XmlWriter xmlWriter, object? value)
    {
        var memoryStream = new MemoryStream();
        xmlSerializer.Serialize(memoryStream, value);
        memoryStream.Seek(0, SeekOrigin.Begin);

        XmlDocument document = new XmlDocument();
        document.Load(memoryStream);
        XmlAttributeCollection attrColl = document.DocumentElement.Attributes;
        attrColl.Remove(attrColl["xsi:type"]);
        attrColl.Remove(attrColl["xmlns:xsd"]);
        attrColl.Remove(attrColl["xmlns:xsi"]);

        document.WriteTo(xmlWriter);
    }
}
namespace Server.Infrastructure;

public class ServerSettingsOptions
{
    public const string ServerSettings = "ServerSettings";
    
    public int DbTimeRequest { get; set; }
}
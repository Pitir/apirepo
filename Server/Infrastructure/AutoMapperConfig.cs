using AutoMapper;
using BaseTypes;
using Server.Data;
using Server.Models.Requests;

namespace Server.Infrastructure;

public class AutoMapperConfig : Profile
{
    public AutoMapperConfig()
    {
        CreateMap<CreateUser, UserInfo>();
        CreateMap<UserInfo, CreateUser>();
        
        CreateMap<User, UserInfo>();
        CreateMap<UserInfo, User>();
    }
}
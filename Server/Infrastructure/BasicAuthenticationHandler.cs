﻿using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Server.Contract;
using Server.Models;

namespace Server.Infrastructure;

public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
{
    private readonly IAuthorizationService _authorizationService;

    public BasicAuthenticationHandler(
        IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger, 
        UrlEncoder encoder, 
        ISystemClock clock, IAuthorizationService authorizationService) 
        : base(options, logger, encoder, clock)
    {
        _authorizationService = authorizationService;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        AuthorizeUser authorizeUser;
        
        try
        {
            if (!Request.Headers.TryGetValue("Authorization", out var authorization))
                return AuthenticateResult.NoResult();
            
            var authHeader = AuthenticationHeaderValue.Parse(authorization);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
            var username = credentials[0];
            var password = credentials[1];
            authorizeUser = await _authorizationService.Authenticate(username, password);
        }
        catch
        {
            return AuthenticateResult.Fail("Error Occured.Authorization failed.");
        }

        var claims = new[]
        {
            new Claim(ClaimTypes.NameIdentifier, authorizeUser.Id),
            new Claim(ClaimTypes.Name, authorizeUser.Username),
        };
 
        var identity = new ClaimsIdentity(claims, Scheme.Name);
        var principal = new ClaimsPrincipal(identity);
 
        var ticket = new AuthenticationTicket(principal, Scheme.Name);
 
        return AuthenticateResult.Success(ticket);
    }
}
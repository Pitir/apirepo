using System.ComponentModel;

namespace Server.Infrastructure;

public static class EnumExtension
{
    public static string GetDescription(this Enum @enum)
    {
        var fi = @enum.GetType().GetField(@enum.ToString());
        DescriptionAttribute[]? attributes = (DescriptionAttribute[]?)fi?.GetCustomAttributes(typeof(DescriptionAttribute), false);
        
        if (attributes is { Length: > 0 })
            return attributes[0].Description;
        
        return @enum.ToString();
    }
}
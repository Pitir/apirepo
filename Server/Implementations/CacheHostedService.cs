using System.Collections.Concurrent;
using BaseTypes;
using Microsoft.Extensions.Options;
using Server.Contract;
using Server.Infrastructure;

namespace Server.Implementations;

public class CacheHostedService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ServerSettingsOptions _serverSettings;

    public CacheHostedService(IServiceProvider serviceProvider, IOptions<ServerSettingsOptions> serverSettings)
    {
        _serviceProvider = serviceProvider;
        _serverSettings = serverSettings.Value;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        Task.Run(async () =>
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                using var scope = _serviceProvider.CreateScope();
                var users = scope.ServiceProvider.GetService<IUserService>()?.GetAll();

                if (users != null)
                {
                    var apiState = scope.ServiceProvider.GetService<IApiState>();
                    apiState.UserInfos = new ConcurrentDictionary<int, UserInfo>(users.ToDictionary(p => p.Id));
                }

                await Task.Delay(_serverSettings.DbTimeRequest, cancellationToken);
            }
        }, cancellationToken);
            
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}
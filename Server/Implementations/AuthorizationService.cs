using Server.Contract;
using Server.Models;

namespace Server.Implementations;

public class AuthorizationService : IAuthorizationService
{
    private string _username = "admin";
    private string _password = "admin";

    public Task<AuthorizeUser> Authenticate(string username, string password)
    {
        if (_username != username || _password != password)
            throw new UnauthorizedAccessException();
        
        return Task.FromResult(new AuthorizeUser {Username = username, Id = "1"});
    }
}
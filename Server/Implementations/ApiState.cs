using System.Collections.Concurrent;
using BaseTypes;
using Server.Contract;

namespace Server.Implementations;

public class ApiState : IApiState
{
    public ConcurrentDictionary<int, UserInfo>  UserInfos { get; set; }
}
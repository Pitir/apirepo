using AutoMapper;
using BaseTypes;
using Microsoft.EntityFrameworkCore;
using Server.Contract;
using Server.Data;
using Server.Models.Responses;

namespace Server.Implementations;

public class UserService : IUserService
{
    private readonly ApiDbContext _apiDbContext;
    private readonly IMapper _mapper;

    public UserService(ApiDbContext apiDbContext, IMapper mapper)
    {
        _apiDbContext = apiDbContext;
        _mapper = mapper;
    }

    public UserInfoResponse Create(UserInfo userInfo)
    {
        var existedUser = _apiDbContext.Users.FirstOrDefault(p => p.Id == userInfo.Id);

        if (existedUser != null)
        {
            return new UserInfoResponse { ErrorCode = ErrorCode.UserAlreadyExist};
        }

        var user = _mapper.Map<User>(userInfo);
        var entity = _apiDbContext.Users.Add(user).Entity;
        
        _apiDbContext.SaveChanges();

        var userInfoResponse = _mapper.Map<UserInfo>(entity);

        return new UserInfoResponse { ErrorCode = ErrorCode.NotError, UserInfo = userInfoResponse };
    }

    public UserInfoResponse SetStatus(int id, Status newStatus)
    {
        var existedUser = _apiDbContext.Users.FirstOrDefault(p => p.Id == id);
        if (existedUser == null)
        {
            return new UserInfoResponse { ErrorCode = ErrorCode.UserNotFound };
        }
        
        existedUser.Status = newStatus;
        _apiDbContext.SaveChanges();
        var userInfoResponse = _mapper.Map<UserInfo>(existedUser);
        
        return new UserInfoResponse { ErrorCode = ErrorCode.NotError, UserInfo = userInfoResponse };
    }

    public UserInfo[] GetAll()
    {
        return _apiDbContext.Users.AsNoTracking().Select(p => _mapper.Map<UserInfo>(p)).ToArray();
    }
}
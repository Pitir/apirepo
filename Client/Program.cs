﻿using BaseTypes;
using Client;
using Client.Models;

const string apiUrl = "https://localhost:7018";

var publicHttpClient = new PublicHttpClient(apiUrl);
var userInfo = await publicHttpClient.GetUserInfoAsync(17);
Console.WriteLine(userInfo);

var authClient = new AuthHttpClient(apiUrl);
var userInfoResponse = await authClient.SetStatusAsync(5, Status.New);

var removeUser = await authClient.RemoveUser(2);

var createUserRequest = new CreateUserRequest
{
    CreateUser = new CreateUser
    {
        Id = 21,
        Name = "Alex",
        Status = Status.New
    }
};
var createUser = await authClient.CreateUser(createUserRequest);
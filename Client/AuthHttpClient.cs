using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using BaseTypes;
using Client.Models;

namespace Client;

public class AuthHttpClient: IDisposable
{
    private readonly HttpClient _httpClient;
    private string _login = "admin";
    private string _password = "admin";

    public AuthHttpClient(string baseUrl)
    {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new Uri(baseUrl);
        
        var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_login}:{_password}"));
        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
    }

    public async Task<UserInfoResponse?> SetStatusAsync(int id, Status newStatus)
    {
        var setStatusRequestMessage = new HttpRequestMessage(HttpMethod.Post, "api/Auth/SetStatus");
        var content = new Dictionary<string, string>
        {
            { "Id", id.ToString() }, 
            { "NewStatus", ((int)newStatus).ToString() }
        };
        setStatusRequestMessage.Content = new FormUrlEncodedContent(content);
        var responseMessage = await _httpClient.SendAsync(setStatusRequestMessage);
        
        Console.WriteLine(await responseMessage.Content.ReadAsStringAsync());
        
        if (responseMessage.IsSuccessStatusCode)
            return await responseMessage.Content.ReadFromJsonAsync<UserInfoResponse>();
        
        return null;
    }

    public async Task<RemoveUserResponse?> RemoveUser(int id)
    {
        var removeUserRequestMessage = new HttpRequestMessage(HttpMethod.Post, "api/Auth/RemoveUser");
        var removeUserRequest = new RemoveUserRequest { RemoveUser = new RemoveUser { Id = 2}};
        removeUserRequestMessage.Content = JsonContent.Create(removeUserRequest);
        var removeUserResponse = await _httpClient.SendAsync(removeUserRequestMessage);
        
        Console.WriteLine(await removeUserResponse.Content.ReadAsStringAsync());

        if (removeUserResponse.IsSuccessStatusCode)
            return await removeUserResponse.Content.ReadFromJsonAsync<RemoveUserResponse>();

        return null;
    }
    
    public async Task<CreateUserResponse?> CreateUser(CreateUserRequest createUserRequest)
    {
        var createUserRequestMessage = new HttpRequestMessage(HttpMethod.Post, "api/Auth/CreateUser");
        var stringXml = XmlHelper.GetStringXml(createUserRequest);
        createUserRequestMessage.Content = new StringContent(stringXml, Encoding.UTF8, "application/xml");
        var createUserResponse = await _httpClient.SendAsync(createUserRequestMessage);

        var stringResponse = await createUserResponse.Content.ReadAsStringAsync();
        Console.WriteLine(stringResponse);

        if (createUserResponse.IsSuccessStatusCode)
            return XmlHelper.Deserialize<CreateUserResponse>(stringResponse);
        
        return null;
    }
    
    public void Dispose()
    {
        _httpClient.Dispose();
    }
}
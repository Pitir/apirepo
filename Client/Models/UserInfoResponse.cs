using BaseTypes;

namespace Client.Models;

public class UserInfoResponse
{
    public ErrorCode ErrorCode { get; set; }
    
    public UserInfo UserInfo { get; set; }
}
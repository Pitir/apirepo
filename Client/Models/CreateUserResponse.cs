using System.Xml.Serialization;

namespace Client.Models;

[XmlRoot(ElementName="Response")]
public class CreateUserResponse
{
    [XmlAttribute(AttributeName="Success")]
    public bool Success { get; set; }
    
    [XmlAttribute(AttributeName="ErrorId")]
    public int ErrorId { get; set; }
    
    [XmlElement(ElementName="ErrorMsg")]
    public string ErrorMsg { get; set; }
    
    [XmlElement(ElementName="user")]
    public CreateUser CreateUser { get; set; }
}
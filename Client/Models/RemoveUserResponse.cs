using BaseTypes;

namespace Client.Models;

public class RemoveUserResponse
{
    public string Msg { get; set; }
    
    public bool Success { get; set; }
    
    public int ErrorId { get; set; }
    
    public UserInfo User { get; set; }

}
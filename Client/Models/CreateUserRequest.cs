using System.Xml.Serialization;

namespace Client.Models;

[XmlRoot(ElementName="Request")]
public class CreateUserRequest 
{
    [XmlElement(ElementName="user")]
    public CreateUser CreateUser { get; set; }
}
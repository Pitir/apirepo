namespace Client;

public class PublicHttpClient : IDisposable
{
    private readonly HttpClient _httpClient;

    public PublicHttpClient(string baseUrl)
    {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new Uri(baseUrl);
    }

    public Task<string> GetUserInfoAsync(int id)
    {
        return _httpClient.GetStringAsync($"Public/UserInfo?id={id}");
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}
using System.Xml;
using System.Xml.Serialization;

namespace Client;

public static class XmlHelper
{
    public static string GetStringXml<T>(T @object)
    {
        string stringXml;
        XmlWriterSettings settings = new XmlWriterSettings
        {
            Indent = true,
            OmitXmlDeclaration = true
        };
        using (StringWriter stringWriter = new StringWriter())
        using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings))
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(xmlWriter, @object, ns);
            stringXml = stringWriter.ToString();
        }

        return stringXml;
    }

    public static T? Deserialize<T>(string xmlString)
    {
        T @object;
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        using (StringReader reader = new StringReader(xmlString))
        {
            @object = (T)serializer.Deserialize(reader);
        }

        return @object;
    }
}